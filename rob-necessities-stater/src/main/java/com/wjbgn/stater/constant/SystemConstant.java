package com.wjbgn.stater.constant;

/**
 * Description: 系统常量
 * Create Date: 2020/6/22
 * @author weirx
 * @version 1.0
 */
public class SystemConstant {

    public final static int ZERO = 0;

    public final static int ONE = 1;
}
