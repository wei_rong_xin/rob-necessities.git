package com.wjbgn.stater.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @description： 订单状态枚举
 * @author：weirx
 * @date：2022/1/27 10:53
 * @version：3.0
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OrderStatusEnum implements IEnum<Integer> {
    NO_PAY(1,"待支付"),
    PAYING(2,"支付中"),
    FINNISH(3,"完成"),
    PAY_FAILED(4,"支付失败");

    private Integer code;

    private String name;

    OrderStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getValue() {
        return code;
    }
}
