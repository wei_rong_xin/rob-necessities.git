package com.wjbgn.stater.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @description： 支付订单状态枚举
 * @author：weirx
 * @date：2022/1/27 10:53
 * @version：3.0
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TradingStatusEnum implements IEnum<Integer> {
    SUCCESS(1, "成功"),
    FAILED(2, "失败");

    private Integer code;

    private String name;

    TradingStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @Override
    public Integer getValue() {
        return code;
    }
}
