package com.wjbgn.account.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.account.dto.UserAccountDTO;
import com.wjbgn.account.entity.UserAccountDO;
import com.wjbgn.stater.util.BeanCopierUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 
 * Create Date: 2022-01-19T15:34:45.686
 * @author weirx
 * @version 1.0
 */
@Component
public class UserAccountDoConvert {

    /**
     * DtoToDo
     *
     * @param userAccountDTO
     * @return
     */
    public static UserAccountDO dtoToDo(UserAccountDTO userAccountDTO) {
            UserAccountDO userAccountDO = new UserAccountDO();
        BeanCopierUtil.copy(userAccountDTO, userAccountDO);
        return userAccountDO;
    }

    /**
     * DoToDto
     *
     * @param userAccountDO
     * @return
     */
    public static UserAccountDTO doToDto(UserAccountDO userAccountDO) {
            UserAccountDTO userAccountDTO = new UserAccountDTO();
        BeanCopierUtil.copy(userAccountDO, userAccountDTO);
        return userAccountDTO;
    }

    /**
     * Page<DO> to Page<DTO>
     *
     * @param pageDO
     * @return
     */
    public static Page<UserAccountDTO> pageConvert(Page<UserAccountDO> pageDO) {
        List<UserAccountDTO> list = listConvert(pageDO.getRecords());
        Page<UserAccountDTO> page = new Page<>(pageDO.getCurrent(), pageDO.getSize(), pageDO.getTotal());
        page.setRecords(list);
        return page;
    }

    /**
     * list<DO> to list<DTO>
     *
     * @param listDO
     * @return
     */
    public static List<UserAccountDTO> listConvert(List<UserAccountDO> listDO) {
        List<UserAccountDTO> list = new ArrayList<>();
            UserAccountDTO userAccountDTO;
        for (UserAccountDO userAccountDO : listDO) {
                userAccountDTO = new UserAccountDTO();
            BeanCopierUtil.copy(userAccountDO, userAccountDTO);
            list.add(userAccountDTO);
        }
        return list;
    }
}