package com.wjbgn.account.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wjbgn.stater.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description:
 * Create Date: 2022-01-19T15:34:45.686
 *
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "UserAccountDTO", description = "数据传输对象UserAccountDTO")
public class UserAccountDTO extends PageDTO {

    public UserAccountDTO(Long userId, Double userAmount, LocalDateTime createTime, Long createUser, Integer isDelete) {
        this.userId = userId;
        this.userAmount = userAmount;
        this.createTime = createTime;
        this.createUser = createUser;
        this.isDelete = isDelete;
    }

    public UserAccountDTO() {
    }

    /**
     *
     */
    @ApiModelProperty(notes = "")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 用户id
     */
    @ApiModelProperty(notes = "用户id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long userId;

    /**
     * 账户总金额
     */
    @ApiModelProperty(notes = "账户总金额")
    private Double userAmount;

    /**
     * 创建时间
     */
    @ApiModelProperty(notes = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(notes = "更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(notes = "创建人")
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(notes = "修改人")
    private Long updateUser;

    /**
     * 是否删除 1：是  0否
     */
    @ApiModelProperty(notes = "是否删除 1：是  0否")
    private Integer isDelete;

    /**
     * 扣减金额
     */
    private Double num;
}
