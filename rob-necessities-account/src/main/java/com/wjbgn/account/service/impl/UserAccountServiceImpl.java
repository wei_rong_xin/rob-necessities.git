package com.wjbgn.account.service.impl;

import com.wjbgn.stater.dto.Result;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.account.entity.UserAccountDO;
import com.wjbgn.account.service.UserAccountService;
import com.wjbgn.account.mapper.UserAccountMapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:34:45.686
 * @author weirx
 * @version 1.0
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccountDO> implements UserAccountService {

    @Override
    public Result accountDeducting(Long userId, Double num) {
        int i = this.baseMapper.accountDeducting(userId,num);
        if (i > 0){
            return Result.success();
        }else {
            return Result.failed("扣减失败或余额不足");
        }
    }
}