package com.wjbgn.account.service;

import com.wjbgn.account.entity.UserAccountDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjbgn.stater.dto.Result;

/**
 * Description: 
 * Create Date: 2022-01-19T15:34:45.686
 * @author weirx
 * @version 1.0
 */
public interface UserAccountService extends IService<UserAccountDO> {

    /**
     * description: 扣减账户金额
     * @param userId
     * @param num
     * @return: com.wjbgn.stater.dto.Result
     * @author: weirx
     * @time: 2022/2/18 11:25
     */
    Result accountDeducting(Long userId, Double num);
}