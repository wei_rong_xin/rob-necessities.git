package com.wjbgn.account.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.account.api.UserClient;
import com.wjbgn.account.api.dto.UserDTO;
import com.wjbgn.account.convert.UserAccountDoConvert;
import com.wjbgn.account.dto.UserAccountDTO;
import com.wjbgn.account.entity.UserAccountDO;
import com.wjbgn.account.service.UserAccountService;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * Create Date: 2022-01-19T15:34:45.686
 *
 * @author weirx
 * @version 1.0
 */
@Api(tags = "")
@RestController
@RequestMapping("/useraccount")
public class UserAccountController {

    /**
     * UserAccountService
     */
    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private UserClient userClient;

    /**
     * 分页列表
     *
     * @param userAccountDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(UserAccountDTO userAccountDTO) {
        QueryWrapper<UserAccountDO> queryWrapper = new QueryWrapper<>(UserAccountDoConvert.dtoToDo(userAccountDTO));
        Page<UserAccountDO> page = new Page<>(userAccountDTO.getPage(), userAccountDTO.getLimit());
        Page<UserAccountDO> pageList = userAccountService.page(page, queryWrapper);
        return Result.success(UserAccountDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     *
     * @param userAccountDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(UserAccountDTO userAccountDTO) {
        QueryWrapper<UserAccountDO> queryWrapper = new QueryWrapper<>(UserAccountDoConvert.dtoToDo(userAccountDTO));
        List<UserAccountDO> userAccountList = userAccountService.list(queryWrapper);
        return Result.success(UserAccountDoConvert.listConvert(userAccountList));
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id") Long id) {
        UserAccountDO userAccount = userAccountService.getById(id);
        return Result.success(null == userAccount ? null : UserAccountDoConvert.doToDto(userAccount));
    }

    /**
     * 新增
     *
     * @param userAccountDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody UserAccountDTO userAccountDTO) {
        boolean flag = userAccountService.save(UserAccountDoConvert.dtoToDo(userAccountDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     *
     * @param userAccountDTO
     * @return
     */
    @ApiOperation(value = "更新")
    @PostMapping("/updateById")
    public Result update(@RequestBody UserAccountDTO userAccountDTO) {
        boolean flag = userAccountService.updateById(UserAccountDoConvert.dtoToDo(userAccountDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     *
     * @param userAccountDTO
     * @return
     */
    @ApiOperation(value = "账户金额扣减")
    @PostMapping("/accountDeducting")
    public Result accountDeducting(@RequestBody UserAccountDTO userAccountDTO) {
        return userAccountService.accountDeducting(userAccountDTO.getUserId(),userAccountDTO.getNum());
    }

    /**
     * 生成测试数据
     */
    @ApiOperation(value = "生成测试用户账户数据")
    @GetMapping("/generateData")
    public void generateData() {
        UserDTO userDTO = new UserDTO();
        userDTO.setLimit(10000);
        for (int i = 1; 1 < 11; i++) {
            userDTO.setPage(i);
            Result list = userClient.pageList(userDTO);
            JSONObject objects = JSONObject.parseObject(JSONObject.toJSONString(list.getData()));
            JSONArray records = objects.getJSONArray("records");
            List<UserAccountDO> userAccountDOS = new ArrayList<>();
            records.forEach(o -> {
                UserDTO user = JSONObject.parseObject(JSONObject.toJSONString(o)).toJavaObject(UserDTO.class);
                UserAccountDO userAccount = new UserAccountDO(user.getId(), 3000d, LocalDateTime.now(), 1L, 0);
                userAccountDOS.add(userAccount);
            });
            userAccountService.saveBatch(userAccountDOS);
        }
    }
}