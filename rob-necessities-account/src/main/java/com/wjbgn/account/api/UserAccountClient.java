package com.wjbgn.account.api;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.account.dto.UserAccountDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Description: 
 * Create Date: 2022-01-19T15:34:45.686
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-account", path = "/useraccount", contextId = "base")
public interface UserAccountClient {

    /**
     * 分页列表
     * @param params
     * @return
     */
    @PostMapping("/pageList")
    Result pageList(@RequestBody Map<String, Object> params);

    /**
     * list列表
     * @param userAccountDTO
     * @return
     */
    @PostMapping("/list")
    Result list(@RequestBody UserAccountDTO userAccountDTO);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     * @param userAccountDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody UserAccountDTO userAccountDTO);

    /**
     * 更新
     * @param userAccountDTO
     * @return
     */
    @PostMapping("/update")
    Result update(@RequestBody UserAccountDTO userAccountDTO);
}