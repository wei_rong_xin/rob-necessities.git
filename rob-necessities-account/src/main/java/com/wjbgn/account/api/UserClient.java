package com.wjbgn.account.api;

import com.wjbgn.account.api.dto.UserDTO;
import com.wjbgn.stater.dto.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Description:
 * Create Date: 2022-01-19T15:24:41.718
 *
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-user", path = "/user", contextId = "base")
public interface UserClient {

    /**
     * 分页列表
     *
     * @param userDTO
     * @return
     */
    @GetMapping("/pageList")
    Result pageList(@SpringQueryMap UserDTO userDTO);

    /**
     * list列表
     *
     * @param userDTO
     * @return
     */
    @GetMapping("/list")
    Result list(@RequestParam UserDTO userDTO);

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     *
     * @param userDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody UserDTO userDTO);

    /**
     * 更新
     *
     * @param userDTO
     * @return
     */
    @PostMapping("/update")
    Result update(@RequestBody UserDTO userDTO);
}