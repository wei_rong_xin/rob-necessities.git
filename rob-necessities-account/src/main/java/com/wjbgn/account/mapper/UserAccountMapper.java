package com.wjbgn.account.mapper;

import com.wjbgn.account.entity.UserAccountDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * Description: 
 * Create Date: 2022-01-19T15:34:45.686
 * @author weirx
 * @version 1.0
 */
@Mapper
public interface UserAccountMapper extends BaseMapper<UserAccountDO> {

    @Update("update rob_user_account set user_amount = user_amount - #{num} where user_id = #{userId} and user_amount >= #{num}")
    int accountDeducting(Long userId,Double num);
}