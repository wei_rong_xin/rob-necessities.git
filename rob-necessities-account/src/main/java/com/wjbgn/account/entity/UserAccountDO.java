package com.wjbgn.account.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description:
 * Create Date: 2022-01-19T15:34:45.686
 *
 * @author weirx
 * @version 1.0
 */
@Data
@TableName(value = "rob_user_account")
public class UserAccountDO {

    /**
     *
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 账户总金额
     */
    private Double userAmount;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 修改人
     */
    private Long updateUser;

    /**
     * 是否删除 1：是  0否
     */
    private Integer isDelete;

    public UserAccountDO(Long userId, Double userAmount, LocalDateTime createTime, Long createUser, Integer isDelete) {
        this.userId = userId;
        this.userAmount = userAmount;
        this.createTime = createTime;
        this.createUser = createUser;
        this.isDelete = isDelete;
    }

    public UserAccountDO() {
    }
}
