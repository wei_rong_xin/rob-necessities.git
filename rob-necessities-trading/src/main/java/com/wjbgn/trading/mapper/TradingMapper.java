package com.wjbgn.trading.mapper;

import com.wjbgn.trading.entity.TradingDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:33:09.329
 * @author weirx
 * @version 1.0
 */
@Mapper
public interface TradingMapper extends BaseMapper<TradingDO> {
}