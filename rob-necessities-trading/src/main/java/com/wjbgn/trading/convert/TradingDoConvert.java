package com.wjbgn.trading.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.trading.dto.TradingDTO;
import com.wjbgn.trading.entity.TradingDO;
import com.wjbgn.stater.util.BeanCopierUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 
 * Create Date: 2022-01-19T15:33:09.329
 * @author weirx
 * @version 1.0
 */
@Component
public class TradingDoConvert {

    /**
     * DtoToDo
     *
     * @param tradingDTO
     * @return
     */
    public static TradingDO dtoToDo(TradingDTO tradingDTO) {
            TradingDO tradingDO = new TradingDO();
        BeanCopierUtil.copy(tradingDTO, tradingDO);
        return tradingDO;
    }

    /**
     * DoToDto
     *
     * @param tradingDO
     * @return
     */
    public static TradingDTO doToDto(TradingDO tradingDO) {
            TradingDTO tradingDTO = new TradingDTO();
        BeanCopierUtil.copy(tradingDO, tradingDTO);
        return tradingDTO;
    }

    /**
     * Page<DO> to Page<DTO>
     *
     * @param pageDO
     * @return
     */
    public static Page<TradingDTO> pageConvert(Page<TradingDO> pageDO) {
        List<TradingDTO> list = listConvert(pageDO.getRecords());
        Page<TradingDTO> page = new Page<>(pageDO.getCurrent(), pageDO.getSize(), pageDO.getTotal());
        page.setRecords(list);
        return page;
    }

    /**
     * list<DO> to list<DTO>
     *
     * @param listDO
     * @return
     */
    public static List<TradingDTO> listConvert(List<TradingDO> listDO) {
        List<TradingDTO> list = new ArrayList<>();
            TradingDTO tradingDTO;
        for (TradingDO tradingDO : listDO) {
                tradingDTO = new TradingDTO();
            BeanCopierUtil.copy(tradingDO, tradingDTO);
            list.add(tradingDTO);
        }
        return list;
    }
}