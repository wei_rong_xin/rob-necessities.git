package com.wjbgn.trading.api;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.trading.dto.TradingDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Description: 
 * Create Date: 2022-01-19T15:33:09.329
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-trading", path = "/trading", contextId = "base")
public interface TradingClient {

    /**
     * 分页列表
     * @param params
     * @return
     */
    @PostMapping("/pageList")
    Result pageList(@RequestBody Map<String, Object> params);

    /**
     * list列表
     * @param tradingDTO
     * @return
     */
    @PostMapping("/list")
    Result list(@RequestBody TradingDTO tradingDTO);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     * @param tradingDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody TradingDTO tradingDTO);

    /**
     * 更新
     * @param tradingDTO
     * @return
     */
    @PostMapping("/update")
    Result update(@RequestBody TradingDTO tradingDTO);
}