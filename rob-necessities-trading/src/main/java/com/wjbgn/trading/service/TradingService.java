package com.wjbgn.trading.service;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.trading.entity.TradingDO;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * Description: 
 * Create Date: 2022-01-19T15:33:09.329
 * @author weirx
 * @version 1.0
 */
public interface TradingService extends IService<TradingDO> {
    Result trading(TradingDO dtoToDo) throws InterruptedException;
}