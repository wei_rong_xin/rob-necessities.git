package com.wjbgn.trading.client;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.trading.client.dto.UserAccountDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Description: 
 * Create Date: 2022-01-19T15:34:45.686
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-account", path = "/useraccount", contextId = "base")
public interface UserAccountClient {

    /**
     * 分页列表
     * @param params
     * @return
     */
    @PostMapping("/pageList")
    Result pageList(@RequestBody Map<String, Object> params);

    /**
     * list列表
     * @param userAccountDTO
     * @return
     */
    @GetMapping("/list")
    Result list(@SpringQueryMap UserAccountDTO userAccountDTO);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     * @param userAccountDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody UserAccountDTO userAccountDTO);

    /**
     * 更新
     * @param userAccountDTO
     * @return
     */
    @PostMapping("/updateById")
    Result update(@RequestBody UserAccountDTO userAccountDTO);

    /**
     * description: 扣减账户金额
     * @param userAccountDTO
     * @return: com.wjbgn.stater.dto.Result
     * @author: weirx
     * @time: 2022/2/18 13:32
     */
    @PostMapping("/accountDeducting")
    Result accountDeducting(@RequestBody UserAccountDTO userAccountDTO);
}