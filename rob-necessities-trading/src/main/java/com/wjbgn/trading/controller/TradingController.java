package com.wjbgn.trading.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.trading.entity.TradingDO;
import com.wjbgn.trading.dto.TradingDTO;
import com.wjbgn.trading.service.TradingService;
import com.wjbgn.trading.convert.TradingDoConvert;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Description: 
 * Create Date: 2022-01-19T15:33:09.329
 * @author weirx
 * @version 1.0
 */
@Api(tags = "")
@RestController
@RequestMapping("/trading")
@AllArgsConstructor
public class TradingController {

    /**
     * TradingService
     */
    private TradingService tradingService;

    /**
     * 分页列表
     * @param tradingDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(TradingDTO tradingDTO) {
        QueryWrapper<TradingDO> queryWrapper = new QueryWrapper<>(TradingDoConvert.dtoToDo(tradingDTO));
        Page<TradingDO> page = new Page<>(tradingDTO.getPage(), tradingDTO.getLimit());
        Page<TradingDO> pageList = tradingService.page(page, queryWrapper);
        return Result.success(TradingDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     * @param tradingDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(TradingDTO tradingDTO) {
        QueryWrapper<TradingDO> queryWrapper = new QueryWrapper<>(TradingDoConvert.dtoToDo(tradingDTO));
        List<TradingDO> tradingList = tradingService.list(queryWrapper);
        return Result.success(TradingDoConvert.listConvert(tradingList));
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id")Long id) {
            TradingDO trading = tradingService.getById(id);
        return Result.success(null == trading ? null : TradingDoConvert.doToDto(trading));
    }

    /**
     * 新增
     * @param tradingDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody TradingDTO tradingDTO) throws InterruptedException {
        return tradingService.trading(TradingDoConvert.dtoToDo(tradingDTO));
    }

    /**
     * 更新
     * @param tradingDTO
     * @return
     */
    @ApiOperation(value = "更新")
    @PostMapping("/update")
    public Result update(@RequestBody TradingDTO tradingDTO) {
        boolean flag = tradingService.updateById(TradingDoConvert.dtoToDo(tradingDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }
}