-- 验证订单金额
select sum(order_amount) from rob_order;
-- 验证订单详情金额
select sum(goods_num * goods_unit_price) from rob_order_detail;
-- 验证支付金额
select sum(trading_amount) from rob_trading;
-- 验证用户账号扣款金额
select COUNT(*) * 3000 - sum(user_amount) from rob_user_account where user_amount < 3000;
-- 验证扣款用户数
select count(*) from rob_user_account where user_amount <3000;
-- 验证商品售出库存
select 1000000* count(*) - sum(goods_inventory) from rob_goods;
-- 验证订单购买商品数量
select sum(goods_num) from rob_order_detail

--
-- 还原商品库存
update rob_goods set goods_inventory = 1000000;
-- 还原用账户金额
update rob_user_account set user_amount = 3000;
-- 清空订单
truncate table rob_order;
truncate table rob_order_detail;
truncate table rob_trading;
