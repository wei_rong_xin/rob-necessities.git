package com.wjbgn.goods.api;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.goods.dto.GoodsDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Description: 
 * Create Date: 2022-01-19T17:10:30.053
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-goods", path = "/goods", contextId = "base")
public interface GoodsClient {

    /**
     * 分页列表
     * @param params
     * @return
     */
    @PostMapping("/pageList")
    Result pageList(@RequestBody Map<String, Object> params);

    /**
     * list列表
     * @param goodsDTO
     * @return
     */
    @PostMapping("/list")
    Result list(@RequestBody GoodsDTO goodsDTO);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     * @param goodsDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody GoodsDTO goodsDTO);

    /**
     * 更新
     * @param goodsDTO
     * @return
     */
    @PostMapping("/update")
    Result update(@RequestBody GoodsDTO goodsDTO);
}