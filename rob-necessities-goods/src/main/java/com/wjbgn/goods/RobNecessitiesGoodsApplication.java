package com.wjbgn.goods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class RobNecessitiesGoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RobNecessitiesGoodsApplication.class, args);
    }

}
