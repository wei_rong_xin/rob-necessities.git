package com.wjbgn.goods.mapper;

import com.wjbgn.goods.entity.GoodsDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * Description: 
 * Create Date: 2022-01-19T17:10:30.053
 * @author weirx
 * @version 1.0
 */
@Mapper
public interface GoodsMapper extends BaseMapper<GoodsDO> {

    @Update("update rob_goods set goods_inventory = goods_inventory - #{num, jdbcType=INTEGER} where id = #{id, jdbcType=BIGINT} and goods_inventory >= #{num, jdbcType=INTEGER}")
    int inventoryDeducting(Long id, Integer num);
}