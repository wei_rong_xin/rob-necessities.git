package com.wjbgn.goods.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.goods.convert.GoodsDoConvert;
import com.wjbgn.goods.dto.GoodsDTO;
import com.wjbgn.goods.entity.GoodsDO;
import com.wjbgn.goods.service.GoodsService;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Description:
 * Create Date: 2022-01-19T17:10:30.053
 *
 * @author weirx
 * @version 1.0
 */
@Api(tags = "")
@RestController
@RequestMapping("/goods")
@AllArgsConstructor
public class GoodsController {

    /**
     * GoodsService
     */
    private GoodsService goodsService;

    /**
     * 分页列表
     *
     * @param goodsDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(GoodsDTO goodsDTO) {
        QueryWrapper<GoodsDO> queryWrapper = new QueryWrapper<>(GoodsDoConvert.dtoToDo(goodsDTO));
        Page<GoodsDO> page = new Page<>(goodsDTO.getPage(), goodsDTO.getLimit());
        Page<GoodsDO> pageList = goodsService.page(page, queryWrapper);
        return Result.success(GoodsDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     *
     * @param goodsDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(GoodsDTO goodsDTO) {
        QueryWrapper<GoodsDO> queryWrapper = new QueryWrapper<>(GoodsDoConvert.dtoToDo(goodsDTO));
        List<GoodsDO> goodsList = goodsService.list(queryWrapper);
        return Result.success(GoodsDoConvert.listConvert(goodsList));
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id") Long id) {
        GoodsDO goods = goodsService.getById(id);
        return Result.success(null == goods ? null : GoodsDoConvert.doToDto(goods));
    }

    /**
     * 新增
     *
     * @param goodsDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody GoodsDTO goodsDTO) {
        boolean flag = goodsService.save(GoodsDoConvert.dtoToDo(goodsDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     *
     * @param goodsDTO
     * @return
     */
    @ApiOperation(value = "根据id更新")
    @PostMapping("/updateById")
    public Result update(@RequestBody GoodsDTO goodsDTO) {
        boolean flag = goodsService.updateById(GoodsDoConvert.dtoToDo(goodsDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 生成商品数据
     */
    @ApiOperation(value = "生成商品数据")
    @GetMapping("/generateGoods")
    public void generateGoods() {
        goodsService.generateGoods();
    }

    /**
     * description: 库存扣减
     *
     * @param goodsDTO
     * @return: com.wjbgn.stater.dto.Result
     * @author: weirx
     * @time: 2022/2/18 10:22
     */
    @PostMapping("/inventoryDeducting")
    public Result inventoryDeducting(@RequestBody GoodsDTO goodsDTO) {
        return goodsService.inventoryDeducting(goodsDTO.getId(), goodsDTO.getNum());
    }
}