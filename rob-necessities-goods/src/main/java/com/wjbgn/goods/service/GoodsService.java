package com.wjbgn.goods.service;

import com.wjbgn.goods.entity.GoodsDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjbgn.stater.dto.Result;

/**
 * Description: 
 * Create Date: 2022-01-19T17:10:30.053
 * @author weirx
 * @version 1.0
 */
public interface GoodsService extends IService<GoodsDO> {

    /**
     * description: 生成测试商品数据

     * @return: void
     * @author: weirx
     * @time: 2022/1/25 14:51
     */
    void generateGoods();

    /**
     * description: 扣减库存
     * @param goodsId
     * @param num
     * @return: com.wjbgn.stater.dto.Result
     * @author: weirx
     * @time: 2022/2/18 10:14
     */
    Result inventoryDeducting(Long goodsId, Integer num);
}