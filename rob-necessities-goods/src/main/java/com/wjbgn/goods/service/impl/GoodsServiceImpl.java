package com.wjbgn.goods.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.goods.entity.GoodsDO;
import com.wjbgn.goods.mapper.GoodsMapper;
import com.wjbgn.goods.service.GoodsService;
import com.wjbgn.stater.dto.Result;
import org.springframework.stereotype.Service;

/**
 * Description:
 * Create Date: 2022-01-19T17:10:30.053
 *
 * @author weirx
 * @version 1.0
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, GoodsDO> implements GoodsService {

    @Override
    public void generateGoods() {
        for (int i = 1; i < 10001; i++) {
            GoodsDO goodsDO = new GoodsDO();
            goodsDO.setGoodsName("goods_" + i);
            goodsDO.setGoodsInventory(1000000);
            goodsDO.setCreateUser(1L);
            goodsDO.setGoodsPrice(RandomUtil.randomDouble(1d, 500d));
            this.save(goodsDO);
        }
    }

    @Override
    public Result inventoryDeducting(Long goodsId, Integer num) {
        int i = this.baseMapper.inventoryDeducting(goodsId, num);
        if (i > 0) {
            return Result.success(this.getById(goodsId));
        } else {
            return Result.failed("库存不足");
        }
    }
}