package com.wjbgn.goods.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.goods.dto.GoodsDTO;
import com.wjbgn.goods.entity.GoodsDO;
import com.wjbgn.stater.util.BeanCopierUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 
 * Create Date: 2022-01-19T17:10:30.053
 * @author weirx
 * @version 1.0
 */
@Component
public class GoodsDoConvert {

    /**
     * DtoToDo
     *
     * @param goodsDTO
     * @return
     */
    public static GoodsDO dtoToDo(GoodsDTO goodsDTO) {
            GoodsDO goodsDO = new GoodsDO();
        BeanCopierUtil.copy(goodsDTO, goodsDO);
        return goodsDO;
    }

    /**
     * DoToDto
     *
     * @param goodsDO
     * @return
     */
    public static GoodsDTO doToDto(GoodsDO goodsDO) {
            GoodsDTO goodsDTO = new GoodsDTO();
        BeanCopierUtil.copy(goodsDO, goodsDTO);
        return goodsDTO;
    }

    /**
     * Page<DO> to Page<DTO>
     *
     * @param pageDO
     * @return
     */
    public static Page<GoodsDTO> pageConvert(Page<GoodsDO> pageDO) {
        List<GoodsDTO> list = listConvert(pageDO.getRecords());
        Page<GoodsDTO> page = new Page<>(pageDO.getCurrent(), pageDO.getSize(), pageDO.getTotal());
        page.setRecords(list);
        return page;
    }

    /**
     * list<DO> to list<DTO>
     *
     * @param listDO
     * @return
     */
    public static List<GoodsDTO> listConvert(List<GoodsDO> listDO) {
        List<GoodsDTO> list = new ArrayList<>();
            GoodsDTO goodsDTO;
        for (GoodsDO goodsDO : listDO) {
                goodsDTO = new GoodsDTO();
            BeanCopierUtil.copy(goodsDO, goodsDTO);
            list.add(goodsDTO);
        }
        return list;
    }
}