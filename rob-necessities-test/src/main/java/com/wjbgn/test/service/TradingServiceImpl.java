package com.wjbgn.test.service;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @description： 支付服务
 * @author：weirx
 * @date：2022/1/27 10:30
 * @version：3.0
 */
@Slf4j
@Component
public class TradingServiceImpl {

    @Value("${server.url.trading}")
    private String tradingServerUrl;

    private final static String TRADING_SAVE = "/trading/save";

    /**
     * description:支付
     *
     * @return: void
     * @author: weirx
     * @time: 2022/1/27 10:31
     */
    public String pay(Long orderId) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("orderId", orderId);
        return HttpUtil.createPost(tradingServerUrl + TRADING_SAVE).body(JSONObject.toJSONString(map)).execute().body();
    }
}
