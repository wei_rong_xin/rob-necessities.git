package com.wjbgn.test.util.kafka.consumer;

import com.wjbgn.test.service.TradingServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * kafka消费者
 *
 * @author weirx
 * @date 2021/02/03 15:01
 **/
@Slf4j
@Component
public class KafkaConsumer {

    @Autowired
    private TradingServiceImpl tradingService;

    @KafkaListener(topics = {"rob-necessities-trading"})
    public void consumer(ConsumerRecord<?, ?> record) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            Object message = kafkaMessage.get();
            String orderId = message.toString();
            log.info("支付开始时间***********************：{}，订单id： {}", LocalDateTime.now(), orderId);
            tradingService.pay(Long.valueOf(orderId));
            log.info("支付完成时间/////////////////////////：{}，订单id： {}", LocalDateTime.now(), orderId);
        }
    }
}
