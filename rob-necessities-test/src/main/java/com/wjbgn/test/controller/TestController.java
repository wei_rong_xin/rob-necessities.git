package com.wjbgn.test.controller;

import com.wjbgn.test.service.OrderServiceImpl;
import com.wjbgn.test.service.TradingServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;

/**
 * @description： 测试controller
 * @author：weirx
 * @date：2022/1/27 13:43
 * @version：3.0
 */
@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private TradingServiceImpl tradingService;

    @GetMapping("/order")
    public void order() {
        orderService.order();
    }

    @GetMapping("/trading")
    public String trading(@RequestParam("orderId") Long orderId) {
        return tradingService.pay(orderId);
    }

    @GetMapping("/concurrent/order")
    public Long concurrentOrder() {
        // 使用cyclicBarrier模拟200线程同时到达
        CyclicBarrier cyclicBarrier = new CyclicBarrier(500);
        for (int i = 0; i < 500; i++) {
            new Thread(() -> {
                try {
                    cyclicBarrier.await();
                    log.info("下单开始时间+++++++++++++++++++++++：{}", LocalDateTime.now());
                    orderService.order();
                    log.info("下单完成时间------------------------：{}", LocalDateTime.now());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        return null;
    }

    @GetMapping("/orderCallBack")
    public void orderCallBack(@RequestParam Long orderId) {
        log.info("支付开始时间***********************：{}，订单id： {}", LocalDateTime.now(), orderId);
        tradingService.pay(orderId);
        log.info("支付完成时间/////////////////////////：{}，订单id： {}", LocalDateTime.now(), orderId);
    }
}
