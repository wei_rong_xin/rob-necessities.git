package com.wjbgn.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description:
 * Create Date: 2022-01-19T15:24:41.718
 *
 * @author weirx
 * @version 1.0
 */
@Data
@TableName(value = "rob_user")
public class UserDO {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 手机号
     */
    private String userPhone;

    /**
     * 1男2女
     */
    private Integer userSex;

    /**
     * 地址
     */
    private String userAddress;

    /**
     * 用户省份
     */
    private Integer userProvince;

    /**
     * 用户城市
     */
    private Integer userCity;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 修改人
     */
    private Long updateUser;

    /**
     * 是否删除 1：是  0否
     */
    private Integer isDelete;

}
