package com.wjbgn.user.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wjbgn.stater.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description: 
 * Create Date: 2022-01-19T15:28:57.429
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "OrderDTO", description = "数据传输对象OrderDTO")
public class OrderDTO extends PageDTO {

        /**
         * 
         */
        @ApiModelProperty(notes = "")
            @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Long id;

        /**
         * 用户id
         */
        @ApiModelProperty(notes = "用户id")
            @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Long userId;

        /**
         * 订单状态 1已下单待支付 2支付中 3完成 4支付失败
         */
        @ApiModelProperty(notes = "订单状态 1已下单待支付 2支付中 3完成 4支付失败")
        private Integer orderStatus;

        /**
         * 订单金额
         */
        @ApiModelProperty(notes = "订单金额")
        private Double orderAmount;

        /**
         * 创建时间
         */
        @ApiModelProperty(notes = "创建时间")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createTime;

        /**
         * 更新时间
         */
        @ApiModelProperty(notes = "更新时间")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime updateTime;

        /**
         * 创建人
         */
        @ApiModelProperty(notes = "创建人")
        private Long createUser;

        /**
         * 修改人
         */
        @ApiModelProperty(notes = "修改人")
        private Long updateUser;

        /**
         * 是否删除 1：是  0否
         */
        @ApiModelProperty(notes = "是否删除 1：是  0否")
        private Integer isDelete;

}
