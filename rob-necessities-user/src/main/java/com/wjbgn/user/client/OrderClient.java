package com.wjbgn.user.client;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.user.client.dto.OrderDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Description:
 * Create Date: 2022-01-19T15:28:57.429
 *
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-order", path = "/order", contextId = "base")
public interface OrderClient {

    /**
     * 分页列表
     *
     * @param orderDTO
     * @return
     */
    @GetMapping("/pageList")
    Result pageList(OrderDTO orderDTO);

    /**
     * list列表
     *
     * @param orderDTO
     * @return
     */
    @GetMapping("/list")
    Result list(OrderDTO orderDTO);

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     *
     * @param orderDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody OrderDTO orderDTO);

    /**
     * 更新
     *
     * @param orderDTO
     * @return
     */
    @PostMapping("/update")
    Result update(@RequestBody OrderDTO orderDTO);
}