package com.wjbgn.user.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wjbgn.stater.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description: 
 * Create Date: 2022-01-19T15:33:09.329
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "TradingDTO", description = "数据传输对象TradingDTO")
public class TradingDTO extends PageDTO {

        /**
         * 
         */
        @ApiModelProperty(notes = "")
            @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Long id;

        /**
         * 订单id
         */
        @ApiModelProperty(notes = "订单id")
            @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Long orderId;

        /**
         * 用户id
         */
        @ApiModelProperty(notes = "用户id")
            @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Long userId;

        /**
         * 交易金额
         */
        @ApiModelProperty(notes = "交易金额")
        private Long tradingAmount;

        /**
         * 交易状态 1成功，2失败
         */
        @ApiModelProperty(notes = "交易状态 1成功，2失败")
        private Integer tradingStatus;

        /**
         * 创建时间
         */
        @ApiModelProperty(notes = "创建时间")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createTime;

        /**
         * 更新时间
         */
        @ApiModelProperty(notes = "更新时间")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime updateTime;

        /**
         * 创建人
         */
        @ApiModelProperty(notes = "创建人")
        private Long createUser;

        /**
         * 修改人
         */
        @ApiModelProperty(notes = "修改人")
        private Long updateUser;

        /**
         * 是否删除 1：是  0否
         */
        @ApiModelProperty(notes = "是否删除 1：是  0否")
        private Integer isDelete;

}
