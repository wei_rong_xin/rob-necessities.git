package com.wjbgn.user.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.user.entity.UserDO;
import com.wjbgn.user.service.UserService;
import com.wjbgn.user.mapper.UserMapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:24:41.718
 * @author weirx
 * @version 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserDO> implements UserService {


    @Override
    public void generateTestUser() {
        for (int i = 1; i < 100001; i++) {
            UserDO userDO = new UserDO();
            userDO.setCreateUser(1L);
            userDO.setUserSex(1);
            userDO.setUsername("user_" + i);
            userDO.setUserPhone("18888888888");
            this.save(userDO);
        }
    }
}