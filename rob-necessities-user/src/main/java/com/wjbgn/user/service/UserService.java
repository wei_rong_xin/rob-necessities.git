package com.wjbgn.user.service;

import com.wjbgn.user.entity.UserDO;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * Description: 
 * Create Date: 2022-01-19T15:24:41.718
 * @author weirx
 * @version 1.0
 */
public interface UserService extends IService<UserDO> {

    /**
     * description: 生成测试用户数据

     * @return: void
     * @author: weirx
     * @time: 2022/1/25 14:16
     */
    void generateTestUser();
}