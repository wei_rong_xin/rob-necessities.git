package com.wjbgn.user.mapper;

import com.wjbgn.user.entity.UserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:24:41.718
 * @author weirx
 * @version 1.0
 */
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {
}