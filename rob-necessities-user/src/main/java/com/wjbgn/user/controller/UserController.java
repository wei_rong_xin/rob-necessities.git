package com.wjbgn.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.stater.dto.Result;
import com.wjbgn.user.convert.UserDoConvert;
import com.wjbgn.user.dto.UserDTO;
import com.wjbgn.user.entity.UserDO;
import com.wjbgn.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Description:
 * Create Date: 2022-01-19T15:24:41.718
 *
 * @author weirx
 * @version 1.0
 */
@Api(tags = "")
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    /**
     * UserService
     */
    private UserService userService;

    /**
     * 分页列表
     *
     * @param userDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(UserDTO userDTO) {
        QueryWrapper<UserDO> queryWrapper = new QueryWrapper<>(UserDoConvert.dtoToDo(userDTO));
        Page<UserDO> page = new Page<>(userDTO.getPage(), userDTO.getLimit());
        Page<UserDO> pageList = userService.page(page, queryWrapper);
        return Result.success(UserDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     *
     * @param userDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(UserDTO userDTO) {
        QueryWrapper<UserDO> queryWrapper = new QueryWrapper<>(UserDoConvert.dtoToDo(userDTO));
        List<UserDO> userList = userService.list(queryWrapper);
        return Result.success(UserDoConvert.listConvert(userList));
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id") Long id) {
        UserDO user = userService.getById(id);
        return Result.success(null == user ? null : UserDoConvert.doToDto(user));
    }

    /**
     * 新增
     *
     * @param userDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody UserDTO userDTO) {
        boolean flag = userService.save(UserDoConvert.dtoToDo(userDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     *
     * @param userDTO
     * @return
     */
    @ApiOperation(value = "更新")
    @PostMapping("/updateById")
    public Result update(@RequestBody UserDTO userDTO) {
        boolean flag = userService.updateById(UserDoConvert.dtoToDo(userDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }


    /**
     * 生成测试用户数数据
     */
    @ApiOperation(value = "生成测试用户数据")
    @GetMapping("/generateTestUser")
    public void generateTestUser() {
        CompletableFuture.runAsync(()->{
            userService.generateTestUser();
        });
    }
}