package com.wjbgn.user.dto;

import com.wjbgn.stater.dto.PageDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * Description: 
 * Create Date: 2022-01-19T15:24:41.718
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "UserDTO", description = "数据传输对象UserDTO")
public class UserDTO extends PageDTO {

        /**
         * 
         */
        @ApiModelProperty(notes = "")
            @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Long id;

        /**
         * 用户名
         */
        @ApiModelProperty(notes = "用户名")
        private String username;

        /**
         * 昵称
         */
        @ApiModelProperty(notes = "昵称")
        private String nickname;

        /**
         * 手机号
         */
        @ApiModelProperty(notes = "手机号")
        private String userPhone;

        /**
         * 1男2女
         */
        @ApiModelProperty(notes = "1男2女")
        private Integer userSex;

        /**
         * 地址
         */
        @ApiModelProperty(notes = "地址")
        private String userAddress;

        /**
         * 用户省份
         */
        @ApiModelProperty(notes = "用户省份")
        private Integer userProvince;

        /**
         * 用户城市
         */
        @ApiModelProperty(notes = "用户城市")
        private Integer userCity;

        /**
         * 创建时间
         */
        @ApiModelProperty(notes = "创建时间")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createTime;

        /**
         * 更新时间
         */
        @ApiModelProperty(notes = "更新时间")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime updateTime;

        /**
         * 创建人
         */
        @ApiModelProperty(notes = "创建人")
        private Long createUser;

        /**
         * 修改人
         */
        @ApiModelProperty(notes = "修改人")
        private Long updateUser;

        /**
         * 是否删除 1：是  0否
         */
        @ApiModelProperty(notes = "是否删除 1：是  0否")
        private Integer isDelete;

}
