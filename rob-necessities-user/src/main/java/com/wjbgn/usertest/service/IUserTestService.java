package com.wjbgn.usertest.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wjbgn.usertest.entity.UserDO;

import java.util.List;

/**
 * @description： 用户服务接口
 * @author：weirx
 * @date：2022/1/17 15:02
 * @version：3.0
 */
public interface IUserTestService extends IService<UserDO> {

    List<UserDO> getList();

    int saveUser(UserDO userDO);
}
