package com.wjbgn.usertest.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.usertest.entity.UserDO;
import com.wjbgn.usertest.mapper.UserTestMapper;
import com.wjbgn.usertest.service.IUserTestService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description： 用户接口实现
 * @author：weirx
 * @date：2022/1/17 15:03
 * @version：3.0
 */
@Service
@DS("slave_1")
public class UserTestTestServiceImpl extends ServiceImpl<UserTestMapper, UserDO> implements IUserTestService {


    @DS("salve_1")
    @Override
    public List<UserDO> getList() {
        return this.getList();
    }

    @DS("master")
    @Override
    public int saveUser(UserDO userDO) {
        boolean save = this.save(userDO);
        if (save){
            return 1;
        }else{
            return 0;
        }
    }
}
