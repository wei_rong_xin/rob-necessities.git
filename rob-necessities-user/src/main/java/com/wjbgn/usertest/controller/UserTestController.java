package com.wjbgn.usertest.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.stater.enums.SexEnum;
import com.wjbgn.usertest.entity.UserDO;
import com.wjbgn.usertest.service.IUserTestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description： 用户controller
 * @author：weirx
 * @date：2022/1/17 17:39
 * @version：3.0
 */
@Api(tags = "mybatis-plus测试接口")
@RestController
@RequestMapping("/userTest")
public class UserTestController {

    @Autowired
    private IUserTestService userService;

    /**
     * description: 新增

     * @return: boolean
     * @author: weirx
     * @time: 2022/1/17 19:11
     */
    @ApiOperation(value = "新增")
    @GetMapping("/save")
    public boolean save() {
        UserDO userDO = new UserDO();
        userDO.setNickname("大漂亮");
        userDO.setSex(SexEnum.MAN);

        return userService.save(userDO);
    }

    /**
     * description: 修改
     * @param nickname
     * @param id
     * @return: boolean
     * @author: weirx
     * @time: 2022/1/17 19:11
     */
    @ApiOperation(value = "修改")
    @GetMapping("/update")
    public boolean update(@RequestParam String nickname,@RequestParam Long id) {
        UserDO userDO = new UserDO();
        userDO.setNickname(nickname);
        userDO.setId(id);
        return userService.updateById(userDO);
    }

    /**
     * description: 删除
     * @param id
     * @return: boolean
     * @author: weirx
     * @time: 2022/1/17 19:11
     */
    @ApiOperation(value = "删除")
    @GetMapping("/delete")
    public boolean delete(@RequestParam Long id) {
        UserDO userDO = new UserDO();
        userDO.setId(id);
        return userService.removeById(userDO);
    }

    /**
     * description: 列表
     * @return: java.util.List<com.wjbgn.user.entity.UserDO>
     * @author: weirx
     * @time: 2022/1/17 19:11
     */
    @ApiOperation(value = "列表")
    @GetMapping("/list")
    public List<UserDO> list() {
        return userService.list();
    }

    /**
     * description: 分页列表
     * @param current
     * @param size
     * @return: com.baomidou.mybatisplus.extension.plugins.pagination.Page
     * @author: weirx
     * @time: 2022/1/17 19:11
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/page")
    public Page page(@RequestParam int current,@RequestParam int size) {
        return userService.page(new Page<>(current,size), new QueryWrapper(new UserDO()));
    }

}
