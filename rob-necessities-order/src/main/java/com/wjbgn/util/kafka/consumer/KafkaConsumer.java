package com.wjbgn.util.kafka.consumer;

import com.alibaba.fastjson.JSONObject;
import com.wjbgn.order.dto.OrderDTO;
import com.wjbgn.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * kafka消费者
 *
 * @author weirx
 * @date 2021/02/03 15:01
 **/
@Slf4j
@Component
public class KafkaConsumer {

    @Autowired
    private OrderService orderService;

    @KafkaListener(topics = {"rob-necessities-order"})
    public void consumer(ConsumerRecord<?, ?> record) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            Object message = kafkaMessage.get();
            log.info("----------------- record =" + record);
            log.info("------------------ message =" + message);
            JSONObject jsonObject = JSONObject.parseObject(message.toString());
            OrderDTO orderDTO = jsonObject.toJavaObject(OrderDTO.class);
            orderService.saveOrder(orderDTO);
        }
    }
}
