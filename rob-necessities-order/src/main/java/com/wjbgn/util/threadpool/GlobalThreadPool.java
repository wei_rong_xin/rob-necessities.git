package com.wjbgn.util.threadpool;

import cn.hutool.core.thread.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @description： 全局通用线程池
 * @author：weirx
 * @date：2021/9/9 18:09
 * @version：3.0
 */
@Slf4j
public class GlobalThreadPool {

    /**
     * 核心线程数
     */
    public final static int CORE_POOL_SIZE = 10;

    /**
     * 最大线程数
     */
    public final static int MAX_NUM_POOL_SIZE = 20;

    /**
     * 任务队列大小
     */
    public final static int BLOCKING_QUEUE_SIZE = 30;

    /**
     * 线程池实例
     */
    private final static ThreadPoolExecutor instance = getInstance();


    /**
     * description: 初始化线程池
     *
     * @return: java.util.concurrent.ThreadPoolExecutor
     * @author: weirx
     * @time: 2021/9/10 9:49
     */
    private synchronized static ThreadPoolExecutor getInstance() {
        // 生成线程池
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_NUM_POOL_SIZE,
                60,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(BLOCKING_QUEUE_SIZE),
                new NamedThreadFactory("rob-necessities-order", false));
        return executor;
    }

    private GlobalThreadPool() {
    }

    public static ThreadPoolExecutor getExecutor() {
        return instance;
    }
}
