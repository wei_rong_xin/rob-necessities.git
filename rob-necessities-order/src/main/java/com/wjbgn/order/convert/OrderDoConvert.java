package com.wjbgn.order.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.order.dto.OrderDTO;
import com.wjbgn.order.entity.OrderDO;
import com.wjbgn.stater.util.BeanCopierUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 
 * Create Date: 2022-01-19T15:28:57.429
 * @author weirx
 * @version 1.0
 */
@Component
public class OrderDoConvert {

    /**
     * DtoToDo
     *
     * @param orderDTO
     * @return
     */
    public static OrderDO dtoToDo(OrderDTO orderDTO) {
            OrderDO orderDO = new OrderDO();
        BeanCopierUtil.copy(orderDTO, orderDO);
        return orderDO;
    }

    /**
     * DoToDto
     *
     * @param orderDO
     * @return
     */
    public static OrderDTO doToDto(OrderDO orderDO) {
            OrderDTO orderDTO = new OrderDTO();
        BeanCopierUtil.copy(orderDO, orderDTO);
        return orderDTO;
    }

    /**
     * Page<DO> to Page<DTO>
     *
     * @param pageDO
     * @return
     */
    public static Page<OrderDTO> pageConvert(Page<OrderDO> pageDO) {
        List<OrderDTO> list = listConvert(pageDO.getRecords());
        Page<OrderDTO> page = new Page<>(pageDO.getCurrent(), pageDO.getSize(), pageDO.getTotal());
        page.setRecords(list);
        return page;
    }

    /**
     * list<DO> to list<DTO>
     *
     * @param listDO
     * @return
     */
    public static List<OrderDTO> listConvert(List<OrderDO> listDO) {
        List<OrderDTO> list = new ArrayList<>();
            OrderDTO orderDTO;
        for (OrderDO orderDO : listDO) {
                orderDTO = new OrderDTO();
            BeanCopierUtil.copy(orderDO, orderDTO);
            list.add(orderDTO);
        }
        return list;
    }
}