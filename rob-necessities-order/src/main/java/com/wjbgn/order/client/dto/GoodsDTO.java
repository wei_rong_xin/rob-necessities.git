package com.wjbgn.order.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wjbgn.stater.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description:
 * Create Date: 2022-01-19T17:10:30.053
 *
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "GoodsDTO", description = "数据传输对象GoodsDTO")
public class GoodsDTO extends PageDTO {

    public GoodsDTO(Long id, String goodsName, Integer goodsInventory, String goodsDescribe, Double goodsPrice, LocalDateTime createTime, LocalDateTime updateTime, Long createUser, Long updateUser, Integer isDelete, Integer num) {
        this.id = id;
        this.goodsName = goodsName;
        this.goodsInventory = goodsInventory;
        this.goodsDescribe = goodsDescribe;
        this.goodsPrice = goodsPrice;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.createUser = createUser;
        this.updateUser = updateUser;
        this.isDelete = isDelete;
        this.num = num;
    }

    public GoodsDTO() {
    }

    /**
     *
     */
    @ApiModelProperty(notes = "")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 商品名称
     */
    @ApiModelProperty(notes = "商品名称")
    private String goodsName;

    /**
     * 库存
     */
    @ApiModelProperty(notes = "库存")
    private Integer goodsInventory;

    /**
     * 商品描述
     */
    @ApiModelProperty(notes = "商品描述")
    private String goodsDescribe;

    /**
     * 商品单价
     */
    @ApiModelProperty(notes = "商品单价")
    private Double goodsPrice;

    /**
     * 创建时间
     */
    @ApiModelProperty(notes = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(notes = "更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(notes = "创建人")
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(notes = "修改人")
    private Long updateUser;

    public GoodsDTO(Long id, Integer num) {
        this.id = id;
        this.num = num;
    }

    /**
     * 是否删除 1：是  0否
     */
    @ApiModelProperty(notes = "是否删除 1：是  0否")
    private Integer isDelete;

    /**
     * 扣减数量
     */
    private Integer num;
}
