package com.wjbgn.order.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.order.convert.OrderDoConvert;
import com.wjbgn.order.dto.OrderDTO;
import com.wjbgn.order.entity.OrderDO;
import com.wjbgn.order.service.OrderService;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Description:
 * Create Date: 2022-01-19T15:28:57.429
 *
 * @author weirx
 * @version 1.0
 */
@Api(tags = "")
@RestController
@RequestMapping("/order")
@AllArgsConstructor
public class OrderController {

    /**
     * OrderService
     */
    private OrderService orderService;

    /**
     * 分页列表
     *
     * @param orderDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(OrderDTO orderDTO) {
        QueryWrapper<OrderDO> queryWrapper = new QueryWrapper<>(OrderDoConvert.dtoToDo(orderDTO));
        Page<OrderDO> page = new Page<>(orderDTO.getPage(), orderDTO.getLimit());
        Page<OrderDO> pageList = orderService.page(page, queryWrapper);
        return Result.success(OrderDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     *
     * @param orderDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(OrderDTO orderDTO) {
        QueryWrapper<OrderDO> queryWrapper = new QueryWrapper<>(OrderDoConvert.dtoToDo(orderDTO));
        List<OrderDO> orderList = orderService.list(queryWrapper);
        return Result.success(OrderDoConvert.listConvert(orderList));
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id") Long id) {
        OrderDO order = orderService.getById(id);
        return Result.success(null == order ? null : OrderDoConvert.doToDto(order));
    }

    /**
     * 新增
     *
     * @param orderDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody OrderDTO orderDTO) {
        return orderService.saveOrder(orderDTO);
    }

    /**
     * 更新
     *
     * @param orderDTO
     * @return
     */
    @ApiOperation(value = "更新")
    @PostMapping("/updateById")
    public Result update(@RequestBody OrderDTO orderDTO) {
        boolean flag = orderService.updateById(OrderDoConvert.dtoToDo(orderDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }
}