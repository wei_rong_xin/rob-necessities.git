package com.wjbgn.order.mapper;

import com.wjbgn.order.entity.OrderDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:28:57.429
 * @author weirx
 * @version 1.0
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderDO> {
}