package com.wjbgn.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.order.client.GoodsClient;
import com.wjbgn.order.client.dto.GoodsDTO;
import com.wjbgn.order.convert.OrderDoConvert;
import com.wjbgn.order.dto.OrderDTO;
import com.wjbgn.order.entity.OrderDO;
import com.wjbgn.order.mapper.OrderMapper;
import com.wjbgn.order.service.OrderService;
import com.wjbgn.orderdetail.entity.OrderDetailDO;
import com.wjbgn.orderdetail.service.OrderDetailService;
import com.wjbgn.stater.dto.Result;
import com.wjbgn.stater.enums.CommonReturnEnum;
import com.wjbgn.stater.enums.OrderStatusEnum;
import com.wjbgn.util.kafka.producer.KafkaProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description:
 * Create Date: 2022-01-19T15:28:57.429
 *
 * @author weirx
 * @version 1.0
 */
@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderDO> implements OrderService {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private KafkaProducer kafkaProducer;

    @Override
    public Result saveOrder(OrderDTO orderDTO) {

        // 下单实现
        Result result = this.saveOrderImpl(orderDTO);
        String orderId = JSONObject.parseObject(JSONObject.toJSONString(result.getData())).getString("id");
        kafkaProducer.send("rob-necessities-trading", orderId);
        return Result.success("下单成功");
    }

    /**
     * description: 下单具体实现
     *
     * @param orderDTO
     * @return: com.wjbgn.stater.dto.Result
     * @author: weirx
     * @time: 2022/2/21 16:31
     */
    public Result saveOrderImpl(OrderDTO orderDTO) {

        GoodsDTO goodsDTO = new GoodsDTO(orderDTO.getGoodsId(), orderDTO.getGoodsNum());
        // 扣减库存
        Result result = goodsClient.inventoryDeducting(goodsDTO);
        if (result.getCode() != CommonReturnEnum.SUCCESS.getCode()) {
            return Result.failed("扣除商品库存失败");
        }

        goodsDTO = JSONObject.toJavaObject(JSONObject.parseObject(JSONObject.toJSONString(result.getData())), GoodsDTO.class);
        //计算订单总金额
        double amount = goodsDTO.getGoodsPrice() * orderDTO.getGoodsNum();
        orderDTO.setOrderAmount(amount);
        orderDTO.setCreateUser(1L);
        orderDTO.setOrderStatus(OrderStatusEnum.NO_PAY);
        OrderDO orderDO = OrderDoConvert.dtoToDo(orderDTO);
        // 保存订单主表
        boolean save = this.save(orderDO);
        if (!save) {
            return Result.failed("生成主订单失败");
        }
        // 处理订单详情数据
        OrderDetailDO orderDetailDO = new OrderDetailDO();
        orderDetailDO.setCreateUser(1L);
        orderDetailDO.setOrderId(orderDO.getId());
        orderDetailDO.setGoodsId(orderDTO.getGoodsId());
        orderDetailDO.setGoodsNum(orderDTO.getGoodsNum());
        orderDetailDO.setGoodsUnitPrice(goodsDTO.getGoodsPrice());
        // 保存订单详情
        boolean detail = orderDetailService.save(orderDetailDO);
        if (!detail) {
            return Result.failed("订单详情保存失败");
        }
        return Result.success(orderDO);
    }
}