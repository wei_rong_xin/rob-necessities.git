package com.wjbgn.order.service;

import com.wjbgn.order.dto.OrderDTO;
import com.wjbgn.order.entity.OrderDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjbgn.stater.dto.Result;

/**
 * Description: 
 * Create Date: 2022-01-19T15:28:57.429
 * @author weirx
 * @version 1.0
 */
public interface OrderService extends IService<OrderDO> {

    Result saveOrder(OrderDTO orderDTO);
}