package com.wjbgn.orderdetail.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.orderdetail.dto.OrderDetailDTO;
import com.wjbgn.orderdetail.entity.OrderDetailDO;
import com.wjbgn.stater.util.BeanCopierUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 
 * Create Date: 2022-01-19T15:36:06.690
 * @author weirx
 * @version 1.0
 */
@Component
public class OrderDetailDoConvert {

    /**
     * DtoToDo
     *
     * @param orderDetailDTO
     * @return
     */
    public static OrderDetailDO dtoToDo(OrderDetailDTO orderDetailDTO) {
            OrderDetailDO orderDetailDO = new OrderDetailDO();
        BeanCopierUtil.copy(orderDetailDTO, orderDetailDO);
        return orderDetailDO;
    }

    /**
     * DoToDto
     *
     * @param orderDetailDO
     * @return
     */
    public static OrderDetailDTO doToDto(OrderDetailDO orderDetailDO) {
            OrderDetailDTO orderDetailDTO = new OrderDetailDTO();
        BeanCopierUtil.copy(orderDetailDO, orderDetailDTO);
        return orderDetailDTO;
    }

    /**
     * Page<DO> to Page<DTO>
     *
     * @param pageDO
     * @return
     */
    public static Page<OrderDetailDTO> pageConvert(Page<OrderDetailDO> pageDO) {
        List<OrderDetailDTO> list = listConvert(pageDO.getRecords());
        Page<OrderDetailDTO> page = new Page<>(pageDO.getCurrent(), pageDO.getSize(), pageDO.getTotal());
        page.setRecords(list);
        return page;
    }

    /**
     * list<DO> to list<DTO>
     *
     * @param listDO
     * @return
     */
    public static List<OrderDetailDTO> listConvert(List<OrderDetailDO> listDO) {
        List<OrderDetailDTO> list = new ArrayList<>();
            OrderDetailDTO orderDetailDTO;
        for (OrderDetailDO orderDetailDO : listDO) {
                orderDetailDTO = new OrderDetailDTO();
            BeanCopierUtil.copy(orderDetailDO, orderDetailDTO);
            list.add(orderDetailDTO);
        }
        return list;
    }
}