package com.wjbgn.orderdetail.mapper;

import com.wjbgn.orderdetail.entity.OrderDetailDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:36:06.690
 * @author weirx
 * @version 1.0
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetailDO> {
}