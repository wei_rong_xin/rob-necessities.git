package com.wjbgn.orderdetail.api;

import com.wjbgn.stater.dto.Result;
import com.wjbgn.orderdetail.dto.OrderDetailDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Description: 
 * Create Date: 2022-01-19T15:36:06.690
 * @author weirx
 * @version 1.0
 */
@FeignClient(name = "rob-necessities-order", path = "/orderdetail", contextId = "base")
public interface OrderDetailClient {

    /**
     * 分页列表
     * @param params
     * @return
     */
    @PostMapping("/pageList")
    Result pageList(@RequestBody Map<String, Object> params);

    /**
     * list列表
     * @param orderDetailDTO
     * @return
     */
    @PostMapping("/list")
    Result list(@RequestBody OrderDetailDTO orderDetailDTO);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @GetMapping("/info/getById")
    Result info(@RequestParam("id") Long id);

    /**
     * 新增
     * @param orderDetailDTO
     * @return
     */
    @PostMapping("/save")
    Result save(@RequestBody OrderDetailDTO orderDetailDTO);

    /**
     * 更新
     * @param orderDetailDTO
     * @return
     */
    @PostMapping("/update")
    Result update(@RequestBody OrderDetailDTO orderDetailDTO);
}