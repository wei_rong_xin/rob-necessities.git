package com.wjbgn.orderdetail.service;

import com.wjbgn.orderdetail.entity.OrderDetailDO;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * Description: 
 * Create Date: 2022-01-19T15:36:06.690
 * @author weirx
 * @version 1.0
 */
public interface OrderDetailService extends IService<OrderDetailDO> {
}