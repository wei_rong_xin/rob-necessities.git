package com.wjbgn.orderdetail.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.orderdetail.entity.OrderDetailDO;
import com.wjbgn.orderdetail.service.OrderDetailService;
import com.wjbgn.orderdetail.mapper.OrderDetailMapper;

/**
 * Description: 
 * Create Date: 2022-01-19T15:36:06.690
 * @author weirx
 * @version 1.0
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetailDO> implements OrderDetailService {

}