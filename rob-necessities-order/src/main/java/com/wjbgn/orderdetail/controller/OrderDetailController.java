package com.wjbgn.orderdetail.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.orderdetail.convert.OrderDetailDoConvert;
import com.wjbgn.orderdetail.dto.OrderDetailDTO;
import com.wjbgn.orderdetail.entity.OrderDetailDO;
import com.wjbgn.orderdetail.service.OrderDetailService;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Description:
 * Create Date: 2022-01-19T15:36:06.690
 *
 * @author weirx
 * @version 1.0
 */
@Api(tags = "")
@RestController
@RequestMapping("/orderdetail")
@AllArgsConstructor
public class OrderDetailController {

    /**
     * OrderDetailService
     */
    private OrderDetailService orderDetailService;

    /**
     * 分页列表
     *
     * @param orderDetailDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(OrderDetailDTO orderDetailDTO) {
        QueryWrapper<OrderDetailDO> queryWrapper = new QueryWrapper<>(OrderDetailDoConvert.dtoToDo(orderDetailDTO));
        Page<OrderDetailDO> page = new Page<>(orderDetailDTO.getPage(), orderDetailDTO.getLimit());
        Page<OrderDetailDO> pageList = orderDetailService.page(page, queryWrapper);
        return Result.success(OrderDetailDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     *
     * @param orderDetailDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(OrderDetailDTO orderDetailDTO) {
        QueryWrapper<OrderDetailDO> queryWrapper = new QueryWrapper<>(OrderDetailDoConvert.dtoToDo(orderDetailDTO));
        List<OrderDetailDO> orderDetailList = orderDetailService.list(queryWrapper);
        return Result.success(OrderDetailDoConvert.listConvert(orderDetailList));
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id") Long id) {
        OrderDetailDO orderDetail = orderDetailService.getById(id);
        return Result.success(null == orderDetail ? null : OrderDetailDoConvert.doToDto(orderDetail));
    }

    /**
     * 新增
     *
     * @param orderDetailDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody OrderDetailDTO orderDetailDTO) {
        boolean flag = orderDetailService.save(OrderDetailDoConvert.dtoToDo(orderDetailDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     *
     * @param orderDetailDTO
     * @return
     */
    @ApiOperation(value = "更新")
    @PostMapping("/update")
    public Result update(@RequestBody OrderDetailDTO orderDetailDTO) {
        boolean flag = orderDetailService.updateById(OrderDetailDoConvert.dtoToDo(orderDetailDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }
}