package com.wjbgn.orderdetail.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wjbgn.stater.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Description:
 * Create Date: 2022-01-19T15:36:06.690
 *
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "OrderDetailDTO", description = "数据传输对象OrderDetailDTO")
public class OrderDetailDTO extends PageDTO {

    /**
     *
     */
    @ApiModelProperty(notes = "")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 订单id
     */
    @ApiModelProperty(notes = "订单id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long orderId;

    /**
     * 货物id
     */
    @ApiModelProperty(notes = "货物id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long goodsId;

    /**
     * 货物数量
     */
    @ApiModelProperty(notes = "货物数量")
    private Integer goodsNum;

    /**
     * 货物单价
     */
    @ApiModelProperty(notes = "货物单价")
    private Double goodsUnitPrice;

    /**
     * 创建时间
     */
    @ApiModelProperty(notes = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(notes = "更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(notes = "创建人")
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(notes = "修改人")
    private Long updateUser;

    /**
     * 是否删除 1：是  0否
     */
    @ApiModelProperty(notes = "是否删除 1：是  0否")
    private Integer isDelete;

}
