package com.wjbgn.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class RobNecessitiesGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(RobNecessitiesGeneratorApplication.class, args);
    }

}
