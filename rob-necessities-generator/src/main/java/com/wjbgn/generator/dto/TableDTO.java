package com.wjbgn.generator.dto;

import com.wjbgn.stater.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Description: 表DTO
 *
 * @author weirx
 * @version 1.0
 */
@Data
@ApiModel(value = "TableDTO", description = "表DTO")
public class TableDTO extends PageDTO {

    /**
     * 表名称
     */
    @ApiModelProperty(notes = "表名称")
    private String tableName;
    /**
     * 存储引擎
     */
    @ApiModelProperty(notes = "存储引擎")
    private String engine;
    /**
     * 表描述
     */
    @ApiModelProperty(notes = "表描述")
    private String tableComment;
    /**
     * 创建时间
     */
    @ApiModelProperty(notes = "创建时间")
    private String createTime;
    /**
     * 表名称数组
     */
    @ApiModelProperty(notes = "创建时间")
    private String[] tableNames;
}
