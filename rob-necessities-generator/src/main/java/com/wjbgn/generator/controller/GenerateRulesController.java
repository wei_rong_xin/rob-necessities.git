package com.wjbgn.generator.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.generator.convert.GenerateRulesDoConvert;
import com.wjbgn.generator.dto.GenerateRulesDTO;
import com.wjbgn.generator.entity.GenerateRulesDO;
import com.wjbgn.generator.service.GenerateRulesService;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * GenerateRules Controller控制器
 *
 * @author weirx
 * @since 2020-07-17T10:10:26.179
 */
@Api(tags = "代码生成规则")
@RestController
@RequestMapping("/generaterules")
@AllArgsConstructor
public class GenerateRulesController {

    /**
     * GenerateRulesService
     */
    private GenerateRulesService generateRulesService;

    /**
     * 分页列表
     *
     * @param generateRulesDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(GenerateRulesDTO generateRulesDTO) {
        QueryWrapper<GenerateRulesDO> queryWrapper = new QueryWrapper<>(GenerateRulesDoConvert.dtoToDo(generateRulesDTO));
        Page<GenerateRulesDO> page = new Page<>(generateRulesDTO.getPage(), generateRulesDTO.getLimit());
        Page<GenerateRulesDO> pageList = generateRulesService.page(page, queryWrapper);
        return Result.success(GenerateRulesDoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     *
     * @param generateRulesDTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(GenerateRulesDTO generateRulesDTO) {
        QueryWrapper<GenerateRulesDO> queryWrapper = new QueryWrapper<>(GenerateRulesDoConvert.dtoToDo(generateRulesDTO));
        List<GenerateRulesDO> generateRulesList = generateRulesService.list(queryWrapper);
        return Result.success(GenerateRulesDoConvert.listConvert(generateRulesList));
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getById")
    public Result info(@RequestParam("id") Long id) {
        GenerateRulesDO generateRules = generateRulesService.getById(id);
        return Result.success(GenerateRulesDoConvert.doToDto(generateRules));
    }

    /**
     * 新增
     *
     * @param generateRulesDTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody GenerateRulesDTO generateRulesDTO) {
        boolean flag = generateRulesService.save(GenerateRulesDoConvert.dtoToDo(generateRulesDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     *
     * @param generateRulesDTO
     * @return
     */
    @ApiOperation(value = "更新")
    @PostMapping("/update")
    public Result update(@RequestBody GenerateRulesDTO generateRulesDTO) {
        boolean flag = generateRulesService.updateById(GenerateRulesDoConvert.dtoToDo(generateRulesDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }
}