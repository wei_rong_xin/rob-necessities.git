package com.wjbgn.generator.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.generator.convert.GenerateRulesDoConvert;
import com.wjbgn.generator.convert.TableDoConvert;
import com.wjbgn.generator.dto.GenerateRulesDTO;
import com.wjbgn.generator.dto.TableDTO;
import com.wjbgn.generator.entity.GenerateRulesDO;
import com.wjbgn.generator.entity.TableDO;
import com.wjbgn.generator.service.GenerateRulesService;
import com.wjbgn.generator.service.GenerateService;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Description: 代码生成控制器
 *
 * @author weirx
 * @version 1.0
 */
@Api(tags = "代码生成controller")
@RestController
@RequestMapping("/generate")
public class GenerateController {

    @Value("${generator.code.path}")
    private String generatorCodePath;

    /**
     * 代码生成service
     */
    @Autowired
    private GenerateService generateService;

    /**
     * 生成规则service
     */
    @Autowired
    private GenerateRulesService generateRulesService;


    /**
     * 分页列表
     *
     * @param tableDTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(@RequestBody TableDTO tableDTO) {
        Page<TableDO> page = new Page<>(tableDTO.getPage(), tableDTO.getLimit());
        Page<TableDO> pageList = generateService.page(page, TableDoConvert.dtoToDo(tableDTO));
        return Result.success(TableDoConvert.pageConvert(pageList));
    }

    /**
     * 代码生成-返回字节
     *
     * @param tableDTO
     * @return
     */
    @ApiOperation(value = "代码生成-返回字节")
    @PostMapping("/generateCode")
    public byte[] generateCode(@RequestBody TableDTO tableDTO) {
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = generateService.generateCode(TableDoConvert.dtoToDo(tableDTO));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (ObjectUtils.isEmpty(outputStream)) {
            return null;
        }
        return outputStream.toByteArray();
    }

    /**
     * 代码生成-返回zip
     *
     * @param tableDTO
     * @return
     */
    @ApiOperation(value = "代码生成-生成zip到项目")
    @PostMapping("/generateCodeZip")
    public void generateCodeZip(@RequestBody TableDTO tableDTO) {
        try {
            ByteArrayOutputStream outputStream = generateService.generateCode(TableDoConvert.dtoToDo(tableDTO));
            FileOutputStream fos = new FileOutputStream(generatorCodePath);
            outputStream.writeTo(fos);
            fos.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 代码预览
     *
     * @param tableName
     * @return
     */
    @ApiOperation(value = "代码预览")
    @GetMapping("/previewCode")
    public Result previewCode(@RequestParam String tableName) {
        List<Map<String, String>> list = generateService.previewCode(tableName);
        if (ObjectUtils.isEmpty(list)) {
            return Result.failed("请先设置生成规则");
        }
        return Result.success(list);
    }

    /**
     * 保存规则
     *
     * @param generateRulesDTO
     * @return
     */
    @ApiOperation(value = "保存规则")
    @PostMapping("/saveRules")
    public Result saveRules(@RequestBody GenerateRulesDTO generateRulesDTO) {
        boolean flag = generateRulesService.saveRules(GenerateRulesDoConvert.dtoToDo(generateRulesDTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * Description: 获取规则
     * Created date: 2020/7/20
     *
     * @return com.cloud.bssp.util.R
     * @author weirx
     */
    @ApiOperation(value = "获取规则")
    @GetMapping("/getRules")
    public Result info() {
        GenerateRulesDO generateRules = generateRulesService.getRules();
        return Result.success(GenerateRulesDoConvert.doToDto(generateRules));
    }
}
