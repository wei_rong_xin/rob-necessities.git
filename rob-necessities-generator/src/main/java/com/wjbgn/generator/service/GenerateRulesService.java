package com.wjbgn.generator.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wjbgn.generator.entity.GenerateRulesDO;

/**
 * Module: GenerateRulesService.java
 *
 * @author weirx
 * @version 1.0
 * @date 2020-07-17T10:00:32.925
 * @Descriptions:
 * @since JDK 1.8
 */
public interface GenerateRulesService extends IService<GenerateRulesDO> {

    /**
     * Description: 获取规则
     * Created date: 2020/7/20
     *
     * @return com.cloud.bssp.generator.entity.GenerateRulesDO
     * @author weirx
     */
    GenerateRulesDO getRules();

    /**
     * Description: 保存规则
     * Created date: 2020/7/20
     *
     * @param generateRulesDO
     * @return boolean
     * @author weirx
     */
    boolean saveRules(GenerateRulesDO generateRulesDO);
}