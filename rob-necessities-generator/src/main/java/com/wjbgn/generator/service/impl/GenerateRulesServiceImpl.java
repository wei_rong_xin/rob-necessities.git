package com.wjbgn.generator.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjbgn.generator.dao.GenerateRulesMapper;
import com.wjbgn.generator.entity.GenerateRulesDO;
import com.wjbgn.generator.service.GenerateRulesService;
import com.wjbgn.stater.constant.SystemConstant;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Module: GenerateRulesServiceImpl.java
 *
 * @author weirx
 * @version 1.0
 * @date 2020-07-17T10:14:54.036
 * @Descriptions:
 * @since JDK 1.8
 */
@Service
@AllArgsConstructor
public class GenerateRulesServiceImpl extends ServiceImpl<GenerateRulesMapper, GenerateRulesDO> implements GenerateRulesService {

    /**
     * 代码生成mapper
     */
    private GenerateRulesMapper generateRulesMapper;

    @Override
    public GenerateRulesDO getRules() {
        List<GenerateRulesDO> generateRulesDOS = generateRulesMapper.selectList(null);
        if (CollectionUtils.isEmpty(generateRulesDOS)) {
            return new GenerateRulesDO();
        }
        return generateRulesDOS.get(0);
    }

    @Override
    public boolean saveRules(GenerateRulesDO generateRulesDO) {
        int count;
        List<GenerateRulesDO> generateRulesDOS = generateRulesMapper.selectList(null);
        if (CollectionUtils.isEmpty(generateRulesDOS)) {
            generateRulesDO.setCreateTime(LocalDateTime.now());
            count = generateRulesMapper.insert(generateRulesDO);
        } else {
            generateRulesDO.setUpdateTime(LocalDateTime.now());
            count = generateRulesMapper.updateById(generateRulesDO);
        }
        if (count > SystemConstant.ZERO) {
            return true;
        } else {
            return false;
        }
    }
}