package com.wjbgn.generator.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjbgn.generator.dao.GenerateMapper;
import com.wjbgn.generator.entity.ColumnDO;
import com.wjbgn.generator.dto.CommonMap;
import com.wjbgn.generator.entity.TableEntityDO;
import com.wjbgn.generator.entity.GenerateRulesDO;
import com.wjbgn.generator.entity.TableColumnDO;
import com.wjbgn.generator.entity.TableDO;
import com.wjbgn.generator.service.GenerateRulesService;
import com.wjbgn.generator.service.GenerateService;
import com.wjbgn.stater.constant.SystemConstant;
import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Description: 代码生成服务实现类
 * Create Date: 2020/7/15
 *
 * @author weirx
 * @version 1.0
 */
@Service
@AllArgsConstructor
public class GenerateServiceImpl implements GenerateService {

    /**
     * generateMapper
     */
    private GenerateMapper generateMapper;

    /**
     * 生成规则service
     */
    private GenerateRulesService generateRulesService;

    @Override
    public TableDO queryTable(String tableName) {
        return generateMapper.queryTable(tableName);
    }

    @Override
    public List<TableColumnDO> queryColumns(String tableName) {
        return generateMapper.queryColumns(tableName);
    }

    @Override
    public List<TableDO> queryAllTable() {
        return generateMapper.queryAllTable();
    }

    @Override
    public Page<TableDO> page(Page<TableDO> page, TableDO tableDO) {
        int count;
        List<TableDO> list;
        //count统计
        if (StringUtils.isEmpty(tableDO.getTableName())) {
            count = generateMapper.countAll();
            if (count == 0) {
                return new Page<>();
            }
            list = generateMapper.pageListAll(page.getSize() * (page.getCurrent() - 1), page.getSize());
        } else {
            count = generateMapper.count(tableDO.getTableName());
            if (count == 0) {
                return new Page<>();
            }
            list = generateMapper.pageList(tableDO.getTableName(),
                    page.getSize() * (page.getCurrent() - 1), page.getSize());
        }
        //分页数据
        page.setRecords(list);
        page.setTotal(count);
        return page;
    }

    @Override
    public ByteArrayOutputStream generateCode(TableDO tableDO) throws IOException {
        // 获取生成规则
        List<GenerateRulesDO> list = generateRulesService.list();
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        // 遍历表名
        for (String tableName : tableDO.getTableNames()) {
            // 根据表名查询，获取表的结构
            TableDO table = generateMapper.queryTable(tableName);
            // 获取表字段
            List<TableColumnDO> columns = generateMapper.queryColumns(tableName);
            // 生成逻辑
            this.generatorCode(table, list.get(0), columns, zip);
        }
        // 关闭流
        IOUtils.closeQuietly(zip);
        return outputStream;

    }

    @Override
    public List<Map<String, String>> previewCode(String tableName) {
        List<GenerateRulesDO> list = generateRulesService.list();
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        TableDO table = generateMapper.queryTable(tableName);
        List<TableColumnDO> columns = generateMapper.queryColumns(tableName);
        List<Map<String, String>> listMap = this.previewList(table, list.get(0), columns);
        return listMap;
    }

    private List<Map<String, String>> previewList(
            TableDO table, GenerateRulesDO generateRule, List<TableColumnDO> columns) {
        // 表信息
        TableEntityDO tableEntityDO = new TableEntityDO();
        tableEntityDO.setTableName(table.getTableName());
        tableEntityDO.setComments(table.getTableComment());
        // 表名转换成Java类名,去除表前缀
        String className;
        if (SystemConstant.ONE == generateRule.getIsIgnorePrefix()) {
            className = tableToJava(tableEntityDO.getTableName(), generateRule.getTablePrefix());
        } else {
            className = tableToJava(tableEntityDO.getTableName(), null);
        }
        tableEntityDO.setClassName(className);
        tableEntityDO.setClassname(StringUtils.uncapitalize(className));
        // 列信息
        List<ColumnDO> columnList = new ArrayList<>();
        for (TableColumnDO tableColumnDO : columns) {
            ColumnDO columnDO = new ColumnDO();
            columnDO.setColumnName(tableColumnDO.getColumnName());
            columnDO.setDataType(tableColumnDO.getDataType());
            columnDO.setComments(tableColumnDO.getColumnComment());
            columnDO.setExtra(tableColumnDO.getExtra());
            columnDO.setColumnDefault(tableColumnDO.getColumnDefault());
            columnDO.setIsNullable(tableColumnDO.getIsNullable());
            columnDO.setColumnType(tableColumnDO.getColumnType());

            // 列名转换成Java属性名
            String attrName = columnToJava(columnDO.getColumnName());
            columnDO.setAttrName(attrName);
            columnDO.setAttrname(StringUtils.uncapitalize(attrName));

            // 列的数据类型，转换成Java类型
            String attrType = CommonMap.javaTypeMap.get(columnDO.getDataType());
            attrType = StringUtils.isBlank(attrType) ? "unknownType" : attrType;
            columnDO.setAttrType(attrType);

            // 是否主键
            if ("PRI".equalsIgnoreCase(tableColumnDO.getColumnKey()) && tableEntityDO.getPk() == null) {
                tableEntityDO.setPk(columnDO);
            }
            columnList.add(columnDO);
        }
        tableEntityDO.setColumns(columnList);

        // 没主键，则第一个字段为主键
        if (tableEntityDO.getPk() == null) {
            tableEntityDO.setPk(tableEntityDO.getColumns().get(0));
        }

        // 设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        // 封装模板数据
        Map<String, Object> map = new HashMap<>();
        map.put("tableName", tableEntityDO.getTableName());
        map.put("comments", tableEntityDO.getComments());
        map.put("pk", tableEntityDO.getPk());
        map.put("className", tableEntityDO.getClassName());
        map.put("classname", tableEntityDO.getClassname());
        map.put("pathName", tableEntityDO.getClassname().toLowerCase());
        map.put("columns", tableEntityDO.getColumns());
        map.put("package", generateRule.getPackageName());
        map.put("author", generateRule.getAuthor());
        map.put("date", LocalDateTime.now());
        map.put("dateStr", DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd HH:mm:ss"));
        map.put("jdk", CommonMap.javaTypeMap.get("jdk"));
        map.put("version", CommonMap.javaTypeMap.get("version"));
        map.put("serviceName", generateRule.getServiceName());
        VelocityContext context = new VelocityContext(map);

        // 获取模板列表
        List<String> templates = getTemplates();
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> dataMap;
        for (String template : templates) {
            dataMap = new LinkedHashMap<>();
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            dataMap.put("key", StringUtils.substringBetween(template, "template/", ".vm"));
            dataMap.put("value", sw.toString());
            list.add(dataMap);
        }
        return list;
    }

    private Map<String, Object> generatorCode(TableDO table, GenerateRulesDO generateRule,
                                              List<TableColumnDO> columns, ZipOutputStream zip) throws IOException {
        // 组装表信息
        TableEntityDO tableEntityDO = new TableEntityDO();
        tableEntityDO.setTableName(table.getTableName());
        tableEntityDO.setComments(table.getTableComment());
        // 表名转换成Java类名,根据规则判断是否去除表前缀
        String className;
        if (SystemConstant.ONE == generateRule.getIsIgnorePrefix()) {
            className = tableToJava(tableEntityDO.getTableName(), generateRule.getTablePrefix());
        } else {
            className = tableToJava(tableEntityDO.getTableName(), null);
        }
        tableEntityDO.setClassName(className);
        tableEntityDO.setClassname(StringUtils.uncapitalize(className));
        // 列信息
        List<ColumnDO> columnList = new ArrayList<>();
        for (TableColumnDO tableColumnDO : columns) {
            ColumnDO columnDO = new ColumnDO();
            columnDO.setColumnName(tableColumnDO.getColumnName());
            columnDO.setDataType(tableColumnDO.getDataType());
            columnDO.setComments(tableColumnDO.getColumnComment());
            columnDO.setExtra(tableColumnDO.getExtra());
            columnDO.setColumnDefault(tableColumnDO.getColumnDefault());
            columnDO.setIsNullable(tableColumnDO.getIsNullable());
            columnDO.setColumnType(tableColumnDO.getColumnType());
            // 列名转换成Java属性名
            String attrName = columnToJava(columnDO.getColumnName());
            columnDO.setAttrName(attrName);
            columnDO.setAttrname(StringUtils.uncapitalize(attrName));

            // 列的数据类型，转换成Java类型
            String attrType = CommonMap.javaTypeMap.get(columnDO.getDataType());
            attrType = StringUtils.isBlank(attrType) ? "unknownType" : attrType;
            columnDO.setAttrType(attrType);

            // 是否主键
            if ("PRI".equalsIgnoreCase(tableColumnDO.getColumnKey()) && tableEntityDO.getPk() == null) {
                tableEntityDO.setPk(columnDO);
            }
            columnList.add(columnDO);
        }
        tableEntityDO.setColumns(columnList);

        // 没主键，则第一个字段为主键
        if (tableEntityDO.getPk() == null) {
            tableEntityDO.setPk(tableEntityDO.getColumns().get(0));
        }

        // 设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        // 封装模板数据
        Map<String, Object> map = new HashMap<>();
        map.put("tableName", tableEntityDO.getTableName());
        map.put("comments", tableEntityDO.getComments());
        map.put("pk", tableEntityDO.getPk());
        map.put("className", tableEntityDO.getClassName());
        map.put("classname", tableEntityDO.getClassname());
        map.put("pathName", tableEntityDO.getClassname().toLowerCase());
        map.put("columns", tableEntityDO.getColumns());
        map.put("package", generateRule.getPackageName());
        map.put("author", generateRule.getAuthor());
        map.put("date", LocalDateTime.now());
        map.put("jdk", CommonMap.javaTypeMap.get("jdk"));
        map.put("version", CommonMap.javaTypeMap.get("version"));
        map.put("serviceName", generateRule.getServiceName());
        VelocityContext context = new VelocityContext(map);

        // 获取模板列表
        List<String> templates = getTemplates();
        Map<String, Object> dataMap = new LinkedHashMap<>();
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            dataMap.put(template, sw);
            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(getFileName(template, tableEntityDO, generateRule.getPackageName())));
                IOUtils.write(sw.toString(), zip, "UTF-8");
                IOUtils.closeQuietly(sw);
                zip.closeEntry();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dataMap;
    }

    /**
     * 列名转换成Java属性名
     */
    public static String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_", "");
    }

    /**
     * 表名转换成Java类名
     */
    public static String tableToJava(String tableName, String tablePrefix) {
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replace(tablePrefix, "");
        }
        return columnToJava(tableName);
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, TableEntityDO tableEntityDO, String packageName) {
        String packagePath = "main" + File.separator + "java" + File.separator;
        if (org.apache.commons.lang3.StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator;
        }
        if (template.contains("Entity.java.vm")) {
            return packagePath + "entity" + File.separator + tableEntityDO.getClassName() + "DO.java";
        }
        if (template.contains("Service.java.vm")) {
            return packagePath + "service" + File.separator + tableEntityDO.getClassName() + "Service.java";
        }
        if (template.contains("ServiceImpl.java.vm")) {
            return packagePath + "service" + File.separator + "impl" + File.separator + tableEntityDO.getClassName() + "ServiceImpl.java";
        }
        if (template.contains("Mapper.java.vm")) {
            return packagePath + "mapper" + File.separator + tableEntityDO.getClassName() + "Mapper.java";
        }
        if (template.contains("Controller.java.vm")) {
            return packagePath + "controller" + File.separator + tableEntityDO.getClassName() + "Controller.java";
        }
        if (template.contains("DTO.java.vm")) {
            return packagePath + "dto" + File.separator + tableEntityDO.getClassName() + "DTO.java";
        }
        if (template.contains("Client.java.vm")) {
            return packagePath + "api" + File.separator + tableEntityDO.getClassName() + "Client.java";
        }
        if (template.contains("Convert.java.vm")) {
            return packagePath + "convert" + File.separator + tableEntityDO.getClassName() + "DoConvert.java";
        }
        if (template.contains("Html.vue.vm")) {
            return "vue/views" + File.separator + tableEntityDO.getClassname() + File.separator + "index.vue";
        }
        if (template.contains("Api.js.vm")) {
            return "vue/api" + File.separator + tableEntityDO.getClassname() + ".js";
        }
        if (template.contains("Groovy.vm")) {
            return "groovy" + File.separator + tableEntityDO.getTableName() + ".groovy";
        }
        return null;
    }

    public static List<String> getTemplates() {
        List<String> templates = new ArrayList<>();
        templates.add("template/Entity.java.vm");
        templates.add("template/Service.java.vm");
        templates.add("template/ServiceImpl.java.vm");
        templates.add("template/Mapper.java.vm");
        templates.add("template/Controller.java.vm");
        templates.add("template/DTO.java.vm");
        templates.add("template/Client.java.vm");
        templates.add("template/Convert.java.vm");
        templates.add("template/Html.vue.vm");
        templates.add("template/Api.js.vm");
        templates.add("template/Groovy.vm");
        return templates;
    }
}
