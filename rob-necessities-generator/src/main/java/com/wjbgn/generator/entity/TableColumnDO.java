package com.wjbgn.generator.entity;

import lombok.Data;

/**
 * Description: 表字段DO
 * @author weirx
 * @version 1.0
 */
@Data
public class TableColumnDO {
    /**
     * 字段名称
     */
    private String columnName;
    /**
     * 字段类型
     */
    private String dataType;
    /**
     * 字段描述
     */
    private String columnComment;
    /**
     * 字段键值
     */
    private String columnKey;
    /**
     * 表扩展信息
     */
    private String extra;
    /**
     * 是否为空yes，no
     */
    private String isNullable;

    /**
     * 默认值
     */
    private String columnDefault;
    /**
     * 字段类型
     */
    private String columnType;
}
