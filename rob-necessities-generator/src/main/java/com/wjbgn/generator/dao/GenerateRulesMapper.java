package com.wjbgn.generator.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjbgn.generator.entity.GenerateRulesDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * GenerateRules Mapper 接口
 * </p>
 *
 * @author weirx
 * @since 2020-07-17T10:00:32.925
 */
@Mapper
public interface GenerateRulesMapper extends BaseMapper<GenerateRulesDO> {
}