package ${package}.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package}.entity.${className}DO;
import ${package}.dto.${className}DTO;
import ${package}.service.${className}Service;
import ${package}.convert.${className}DoConvert;
import com.wjbgn.stater.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Description: ${comments}
 * Create Date: ${date}
 * @author ${author}
 * @version ${version}
 */
@Api(tags = "${comments}")
@RestController
@RequestMapping("/${pathName}")
@AllArgsConstructor
public class ${className}Controller {

    /**
     * ${className}Service
     */
    private ${className}Service ${classname}Service;

    /**
     * 分页列表
     * @param ${classname}DTO
     * @return
     */
    @ApiOperation(value = "分页列表")
    @GetMapping("/pageList")
    public Result pageList(${className}DTO ${classname}DTO) {
        QueryWrapper<${className}DO> queryWrapper = new QueryWrapper<>(${className}DoConvert.dtoToDo(${classname}DTO));
        Page<${className}DO> page = new Page<>(${classname}DTO.getPage(), ${classname}DTO.getLimit());
        Page<${className}DO> pageList = ${classname}Service.page(page, queryWrapper);
        return Result.success(${className}DoConvert.pageConvert(pageList));
    }

    /**
     * list列表
     * @param ${classname}DTO
     * @return
     */
    @ApiOperation(value = "list列表")
    @GetMapping("/list")
    public Result list(${className}DTO ${classname}DTO) {
        QueryWrapper<${className}DO> queryWrapper = new QueryWrapper<>(${className}DoConvert.dtoToDo(${classname}DTO));
        List<${className}DO> ${classname}List = ${classname}Service.list(queryWrapper);
        return Result.success(${className}DoConvert.listConvert(${classname}List));
    }

    /**
     * 根据主键查询
     * @param ${pk.attrname}
     * @return
     */
    @ApiOperation(value = "根据主键查询")
    @GetMapping("/info/getBy${pk.attrName}")
    public Result info(@RequestParam("${pk.attrname}")Long ${pk.attrname}) {
            ${className}DO ${classname} = ${classname}Service.getById(id);
        return Result.success(null == ${classname} ? null : ${className}DoConvert.doToDto(${classname}));
    }

    /**
     * 新增
     * @param ${classname}DTO
     * @return
     */
    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody ${className}DTO ${classname}DTO) {
        boolean flag = ${classname}Service.save(${className}DoConvert.dtoToDo(${classname}DTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }

    /**
     * 更新
     * @param ${classname}DTO
     * @return
     */
    @ApiOperation(value = "根据id更新")
    @PostMapping("/updateById")
    public Result update(@RequestBody ${className}DTO ${classname}DTO) {
        boolean flag = ${classname}Service.updateById(${className}DoConvert.dtoToDo(${classname}DTO));
        if (flag) {
            return Result.success();
        }
        return Result.failed();
    }
}